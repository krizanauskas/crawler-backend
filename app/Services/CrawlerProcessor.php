<?php

namespace App\Services;

use App\Events\CrawlerProgressUpdated;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Log;

class CrawlerProcessor
{
    protected $client, $progress_event = false;
    protected $response = null;

    public function __construct()
    {
        $this->client = new Client;
    }

    public function withProgressEvent()
    {
        $this->progress_event = true;

        return $this;
    }

    public function crawl(String $url)
    {
        $this->response = $this->client->request('GET', $url,[
            'progress' => function($download_total, $downloaded_bytes) {
                if ($this->progress_event) {
                    $percent = $this->calculateDownloadPercent($downloaded_bytes, $download_total);
                    $this->cacheCurrentProgress($percent);
                }
            }
        ]);

        return $this;
    }

    public function getContent()
    {
        return $this->response->getBody()->getContents();
    }

    private function calculateDownloadPercent($downloaded_bytes, $download_total)
    {
        $percent = '0%';

        if ($download_total < 1) {
            return $percent;
        } else {
            $percent = (int) (($downloaded_bytes / $download_total) * 100);

            return (string) $percent."%";
        }
    }

    private function cacheCurrentProgress($value)
    {
        $progress = Cache::get('request-download-progress');
        Cache::put('request-download-progress', $value);
        
        if ($progress !== $value) {
            event(new CrawlerProgressUpdated());
        }
    }
}
