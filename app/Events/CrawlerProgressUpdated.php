<?php

namespace App\Events;

use App\Models\Crawler;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class CrawlerProgressUpdated implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('crawling-progress-channel');
    }

    public function broadcastAs()
    {
        return 'crawling-progress-updated';
    }

    public function broadcastWith()
    {
        $percent = Cache::get('request-download-progress');
        $job_id = Cache::get('current-job-id');
        $current_index = Cache::get('current-url-index');
        $total_urls = Cache::get('total-urls');
        $is_completed = Cache::get('current-job-completed');
        return [
            'percent' => $percent,
            'job_id' => $job_id,
            'current_index' => $current_index,
            'total_urls' => $total_urls,
            'is_completed' => $is_completed,
        ];
    }
}
