<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiFormRequest;

class CrawlerCreateRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'selector' => 'required|string|min:1|max:100',
            'urls' => 'required|array|min:1|max:100',
            'urls.*' => 'required|string|min:1|max:100|distinct|url'
        ];
    }
}
