<?php

namespace App\Http\Controllers;

use App\Models\Url;

class UrlController extends ApiController
{
    public function list()
    {
        $urls = Url::limit(100)->orderBy('created_at', 'DESC')->get();

        return $this->respondWithData(['urls' => $urls]);
    }
}
