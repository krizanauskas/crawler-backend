<?php

namespace App\Http\Controllers;

use App\Http\Requests\CrawlerCreateRequest;
use App\Jobs\ProcessCrawler;
use App\Models\Crawler;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CrawlerController extends ApiController
{
    use DispatchesJobs;

    public function create(CrawlerCreateRequest $request)
    {
        $crawler = Crawler::create($request->only('urls', 'selector'));

        $job = (new ProcessCrawler($crawler));
        $job_id = $this->dispatch($job);

        return $this->respondWithData(['job_id' => $job_id]);
    }

    public function list()
    {
        $crawlers = Crawler::limit(100)->orderBy('id', 'DESC')->get();

        return $this->respondWithData(['crawlers' => $crawlers]);
    }
}
