<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

abstract class ApiController extends Controller
{
    public function respondWithData(array $data)
    {
        $response_data = [
            'success' => true,
            'data' => $data
        ];

        return response()->json($response_data, JsonResponse::HTTP_OK);
    }

    public function respondWithError(string $message = '')
    {
        $response_data = [
            'success' => false,
            'message' => $message
        ];

        return response()->json($response_data, JsonResponse::HTTP_BAD_REQUEST);
    }

    public function respondWithSuccess(string $message = '')
    {
        $response_data = [
            'success' => true,
            'message' => $message
        ];

        return response()->json($response_data, JsonResponse::HTTP_OK);
    }
}
