<?php

namespace App\Jobs;

use App\Models\Crawler;
use App\Services\CrawlerProcessor;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Log;

class ProcessCrawler implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $crawler, $domCrawler;
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Crawler $crawler)
    {
        $this->crawler = $crawler;
        $this->domCrawler = new DomCrawler;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CrawlerProcessor $processor)
    {

        $this->crawler->setStarted();
        $urls_to_parse = $this->crawler->getUrls();

        foreach ($urls_to_parse as $key => $url_to_parse) {
            $this->cacheCurrentJobStatus($key);
            $this->crawler->setCurrentIndex($key);

            try {
                $html_content = $processor->withProgressEvent()->crawl($url_to_parse)->getContent();

                [$title, $paragraphs] = $this->parseHtml($html_content, $this->crawler->selector);

                $this->crawler->createUrlModel($url_to_parse, $title, $paragraphs);
            } catch (Exception $ex) {
                Log::debug($ex->getMessage());
            }
        }

        $this->crawler->setCompleted();
    }

    private function parseHtml(String $html_content, String $selector)
    {
        $this->domCrawler->clear();
        $this->domCrawler->addHtmlContent($html_content);

        $title = $this->domCrawler->filter('title')->text();
        $paragraphs = collect($this->domCrawler->filter($selector)->each(function ($node) {
            return $node->text();
        }))->implode(' ');

        return [$title, $paragraphs];
    }

    private function cacheCurrentJobStatus($key)
    {
        $job_id = $this->job->getJobId();
        $index = ++$key;
        $total_urls = count($this->crawler->urls);

        Cache::put('current-job-completed', false);
        Cache::put('current-job-id', $job_id);
        Cache::put('current-url-index', $index);
        Cache::put('total-urls', $total_urls);
    }
}
