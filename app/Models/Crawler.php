<?php

namespace App\Models;

use App\Events\CrawlerProgressUpdated;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Crawler extends Model
{
    use HasFactory;

    const STATUS_PENDING = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_COMPLETED = 'completed';
    const STATUS_FAILED = 'failed';

    protected $casts = [
        'urls' => 'array',
    ];

    protected $fillable = [
        'urls',
        'selector'
    ];

    public function urls()
    {
        return $this->hasMany(Url::class);
    }

    public function getUrls()
    {
        return $this->urls;
    }

    public function setStarted()
    {
        $this->status = self::STATUS_PROCESSING;
        $this->save();
    }

    public function setCurrentIndex(int $current_index)
    {
        $this->current_index = $current_index;
        $this->save();
    }

    public function createUrlModel(String $url, String $title, String $content)
    {
        $this->urls()->create(['url' => $url, 'title' => $title, 'content' => $content]);
    }

    public function setCompleted()
    {
        $this->status = self::STATUS_COMPLETED;
        $this->save();

        Cache::put('current-job-completed', true);
        Cache::put('request-download-progress', '100%');
        event(new CrawlerProgressUpdated());
    }
}
