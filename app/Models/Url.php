<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    use HasFactory;

    protected $fillable = [
        'url',
        'title',
        'content'
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s'
    ];
}
